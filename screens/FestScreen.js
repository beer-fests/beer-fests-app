import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import BeerOverview from '../components/beers/BeerOverview'

export default class FestScreen extends React.Component {
  static navigationOptions = {
    title: 'Bier & Big',
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <BeerOverview onTap={false} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDA92C',
  },
})
