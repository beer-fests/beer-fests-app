import React from 'react'
import { ScrollView, StyleSheet } from 'react-native'
import BeerOverview from '../components/beers/BeerOverview'

export default class BeersScreen extends React.Component {
  static navigationOptions = {
    title: 'onTap',
  }

  render() {
    return (
      <ScrollView style={styles.container}>
        <BeerOverview onTap={true} />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDA92C',
  },
})
