export const typeDefs = `

  type Beer {
    id: ID!
    name: String!
    brewery: Brewery!
    collab(...): [Brewery!]
    type: String
    precentage: String
    description: String
    onTap: Boolean!
    special: Boolean!
    isLiked: Boolean
  }

  type Mutation {
    toggleLikedBeer(id: ID!): Boolean
  }

  type Query {
    likedBeers: [Beer]
  }
`;
