import { gql } from "apollo-boost";

export const defaults = {
  likedBeers: []
};

export const resolvers = {
  Beer: {
    isLiked: () => false
  },
  Mutation: {
    toggleLikedBeer: (_, { id }, { cache, getCacheKey }) => {
      const fragment = gql`
        fragment isLiked on Beer {
          isLiked
          name
        }
      `;
      const fragmentId = getCacheKey({ id, __typename: "Beer" });
      const beer = cache.readFragment({
        fragment,
        id: fragmentId
      });

      // first we have to toggle the client-side only field
      cache.writeData({
        id: fragmentId,
        data: {
          ...beer,
          isLiked: !beer.isLiked
        }
      });

      const query = gql`
        {
          likedBeers @client {
            name
            id
          }
        }
      `;
      const { likedBeers } = cache.readQuery({ query });

      // if we're unliking the beer, remove it from the array.
      const data = {
        likedBeers: beer.isLiked
          ? likedBeers.filter(beer => beer.id !== id)
          : likedBeers.concat([
            { name: beer.name, id, __typename: "LikedBeer" }
          ])
      };

      // add the liked beer to an array for easy access
      cache.writeData({ data });
      return data;
    }
  }
};
