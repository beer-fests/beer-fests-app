import 'react-native';
import React from 'react';
import { Fetching, Error } from '../Fetching';
import renderer from 'react-test-renderer';

it('renders Fetching correctly', () => {
  const tree = renderer.create(<Fetching />).toJSON();

  expect(tree).toMatchSnapshot();
});

it('renders Error correctly', () => {
  const tree = renderer.create(<Error />).toJSON();

  expect(tree).toMatchSnapshot();
});
