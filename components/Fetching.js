import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'

const Fetching = () => (
  <View>
    <Text style={styles.fetching}>🍻 Fetching...</Text>
  </View>
)

const Error = () => (
  <View style={styles.container}>
    <Text style={styles.fetching}>Oh no! Something went wrong.</Text>
    <Image
      source={require('../assets/images/error.png')}
      style={styles.image}
    />
    <Text style={styles.fetching}>Please refresh and try again.</Text>
  </View>
)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  fetching: {
    fontSize: 30,
    margin: 10,
    letterSpacing: 1,
  },
  image: {
    width: 370,
  },
})

export { Fetching, Error }
