import React, { Component } from 'react'
import { SectionList, StyleSheet, View } from 'react-native'

import { gql } from 'apollo-boost'
import { Query } from 'react-apollo'

import { BoothHeader, BoothItem } from './Booth'
import { Fetching, Error } from '../Fetching'

import { pollIntervalInSeconds } from '../../config';

const GET_BOOTHS = gql`
  {
    booths {
      id
      name
      country: beers(first: 1) {
        brewery {
          country
        }
      }
      data: beers {
        id
        name
        type
        precentage
        onTap
        special
        brewery {
          id
          name
        }
      }
    }
  }
`

const GET_BOOTHS_ON_TAP = gql`
  {
    booths {
      id
      name
      country: beers(first: 1) {
        brewery {
          country
        }
      }
      data: beers(where: { onTap: true }) {
        id
        name
        type
        precentage
        onTap
        special
        brewery {
          id
          name
        }
      }
    }
  }
`

class BeerOverview extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Query query={this.props.onTap ? GET_BOOTHS_ON_TAP : GET_BOOTHS} pollInterval={pollIntervalInSeconds*1000}>
          {({ loading, error, data, client }) => {
            if (loading) return <Fetching />
            if (error) return <Error />
            return (
              <SectionList
                sections={data.booths}
                renderItem={({ item }) => <BoothItem item={item || []} />}
                renderSectionHeader={({ section }) => (
                  <BoothHeader section={section} />
                )}
                keyExtractor={(item, index) => index}
              />
            )
          }}
        </Query>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 22,
  },
})

export default BeerOverview
