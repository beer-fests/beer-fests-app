import React, { Component } from 'react'
import { View, StyleSheet, Text } from 'react-native'

export class BoothHeader extends Component {
  render() {
    return (
      <View style={styles.sectionHeader}>
        <View style={styles.booth}>
          <Text style={styles.boothName}>{this.props.section.name}</Text>
        </View>
        <Text style={styles.country}>
          {this.props.section.country[0].brewery.country}
        </Text>
      </View>
    )
  }
}

export class BoothItem extends Component {
  render() {
    const beer = this.props.item
    return (
      <View style={beer.onTap ? styles.beerOnTap : styles.beer}>
        <View style={beer.onTap ? styles.onTap : styles.tap} />
        <View style={styles.description}>
          <Text style={styles.name}>
            {beer.special && !beer.onTap ? 'special' : beer.name}
          </Text>
          <Text style={styles.type}>{beer.type}</Text>
        </View>
        <Text style={styles.precentage}>{beer.precentage}</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  sectionHeader: {
    flex: 1,
    flexDirection: 'row',
  },
  boothName: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: 14,
    fontWeight: 'bold',
  },
  booth: {
    backgroundColor: 'rgba(247,247,247,1.0)',
    width: '70%',
  },
  country: {
    fontSize: 12,
    color: 'rgba(76,76,76,1)',
    backgroundColor: 'rgba(187,187,187,1.0)',
    width: '100%',
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
  },
  beer: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    height: 54,
  },
  beerOnTap: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
    height: 54,
    backgroundColor: 'rgba(218,119,19,1.0)',
  },
  tap: {
    width: '5%',
  },
  onTap: {
    width: '5%',
  },
  description: {
    width: '80%',
  },
  name: {
    fontSize: 18,
  },
  type: {
    paddingLeft: 10,
    fontSize: 12,
    color: 'rgba(66,66,66,1)',
  },
  precentage: {
    paddingRight: 10,
    fontSize: 20,
    color: 'rgba(20,20,20,1)',
    textAlign: 'right',
  },
})
