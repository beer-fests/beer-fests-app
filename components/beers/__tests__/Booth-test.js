import 'react-native'
import React from 'react'
import { BoothHeader, BoothItem } from '../Booth'
import renderer from 'react-test-renderer'

it('renders BoothHeader correctly', () => {
  const booth = {
    name: 'test',
    country: [
      {
        brewery: {
          country: 'Belgie',
        },
      },
    ],
  }
  const tree = renderer.create(<BoothHeader section={booth} />).toJSON()

  expect(tree).toMatchSnapshot()
})

it('renders BoothItem correctly', () => {
  const beer = {
    onTap: true,
    description: 'Test Beer',
    name: 'Test Beer Name',
    type: 'Sour',
    precentage: '9,5%',
    special: false,
  }
  const tree = renderer.create(<BoothItem item={beer} />).toJSON()

  expect(tree).toMatchSnapshot()
})
