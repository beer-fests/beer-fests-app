import React from 'react'

import { Image, Text, StyleSheet, View } from 'react-native'

export class FestListItem extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      fest: this.props.fest,
    }
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.festContainer}>
          <Image source={this.state.fest.image} style={styles.festImage} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDA92C',
  },
  festContainer: {
    alignItems: 'center',
  },
  festImage: {
    width: '100%',
    height: 200,
  },
})

export default FestListItem
