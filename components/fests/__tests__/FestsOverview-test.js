import 'react-native'
import React from 'react'
import FestsOverview from '../FestsOverview'
import renderer from 'react-test-renderer'

it('renders FestsOverview correctly', () => {
  const tree = renderer.create(<FestsOverview />).toJSON()

  expect(tree).toMatchSnapshot()
})
