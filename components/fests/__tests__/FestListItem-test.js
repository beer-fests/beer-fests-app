import 'react-native';
import React from 'react';
import { FestListItem } from '../FestListItem';
import renderer from 'react-test-renderer';

it('renders FestListItem correctly', () => {
  const fest = {
    image: require('../../../assets/images/fests/Eindhoven_van-Moll-fest-1a.jpg'),
    active: false,
  }
  const tree = renderer.create(<FestListItem fest={fest}/>).toJSON();

  expect(tree).toMatchSnapshot();
});
