import React from 'react'

import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native'
import { FestListItem } from './FestListItem'
import Data from '../../constants/Data'

export class FestsOverview extends React.Component {
  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        {Data.fests.map(fest => (
          <FestListItem key={fest.id} fest={fest} />
        ))}
        <View style={styles.helpContainer}>
          <TouchableOpacity
            onPress={this._handleHelpPress}
            style={styles.helpLink}
          >
            <Text style={styles.helpLinkText}>
              Help, it didn’t automatically reload!
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EDA92C',
  },
  festContainer: {
    alignItems: 'center',
  },
  festImage: {
    width: '100%',
    height: 200,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 15,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
})

export default FestsOverview
